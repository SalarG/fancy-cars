import React, {Component} from 'react';
import {View, Text, Image, FlatList, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import data from './data/data';
import LandingPage from './containers/LandingPage/LandingPage';
// this component will be used to intialize listenere, navigation, redux, ..

class Content extends Component {
  render() {
    return (
      <View style={styles.flexOne}>
        <LandingPage />
      </View>
    );
  }
}

export default Content;

const styles = StyleSheet.create({
  flexOne: {
    // flex: 1,
  },
});
