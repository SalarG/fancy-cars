import data from '../data/data';
import availabilities from '../data/availability';

//@GETMapping
class ServerModule {
  static GET = (path, params) => {
    switch (path) {
      case '/cars':
        return data;
      case `/availability`:
        let available = availabilities.find(availability => {
          return availability.id === params;
        }).available;
        return available;
    }
  };
}
export default ServerModule;
