export default [
  {
    id: 1,
    name: 'Mario Speedwagon',
    make: 'Audi',
    model: 'A6',
    year: '2018',
    imageUri:
      'https://images.pexels.com/photos/305070/pexels-photo-305070.jpeg?cs=srgb&dl=auto-automobile-automotive-car-305070.jpg&fm=jpg',
  },
  {
    id: 2,
    name: 'Petey Cruiser',
    make: 'BMW',
    model: '335',
    year: '2019',
    imageUri:
      'https://images.pexels.com/photos/170811/pexels-photo-170811.jpeg?cs=srgb&dl=blue-bmw-sedan-near-green-lawn-grass-170811.jpg&fm=jpg',
  },
  {
    id: 3,
    name: 'Anna Sthesia',
    make: 'Aston Martin',
    model: 'DBZ',
    year: '2011',
    imageUri:
      'https://images.pexels.com/photos/210019/pexels-photo-210019.jpeg?cs=srgb&dl=action-asphalt-auto-automobile-210019.jpg&fm=jpg',
  },
  {
    id: 4,
    name: 'Paul Molive',
    make: 'Toyota',
    moder: 'Land Crousier',
    year: '2016',
    imageUri:
      'https://images.pexels.com/photos/1149137/pexels-photo-1149137.jpeg?cs=srgb&dl=4k-wallpaper-automobile-automotive-branding-1149137.jpg&fm=jpg',
  },
  {
    id: 5,
    name: 'Anna Mull',
    make: 'Dodge',
    model: 'Wiper',
    year: '2019',
    imageUri:
      'https://images.pexels.com/photos/1534604/pexels-photo-1534604.jpeg?cs=srgb&dl=photo-of-classic-car-1534604.jpg&fm=jpg',
  },
  {
    id: 6,
    name: 'Gail Forcewind',
    make: 'Mecedes Benz',
    model: 'SLR 55 AMG',
    year: '2012',
    imageUri:
      'https://images.pexels.com/photos/241316/pexels-photo-241316.jpeg?cs=srgb&dl=alloy-asphalt-auto-automobile-241316.jpg&fm=jpg',
  },
  {
    id: 7,
    name: 'Paige Turner',
    make: 'Range Rover',
    model: 'Velar',
    year: '2013',
    imageUri:
      'https://images.pexels.com/photos/116675/pexels-photo-116675.jpeg?cs=srgb&dl=car-vehicle-automobile-range-rover-116675.jpg&fm=jpg',
  },
  {
    id: 8,
    name: 'Pete Sariya',
    make: 'BMW',
    model: 'M4',
    imageUri:
      'https://images.pexels.com/photos/707046/pexels-photo-707046.jpeg?cs=srgb&dl=white-bmw-e46-under-cloudy-skies-707046.jpg&fm=jpg',
  },
  {
    id: 9,
    name: 'Monty Carlo',
    make: 'Volkswagen',
    model: 'Bettle',
    year: '2015',
    imageUri:
      'https://images.pexels.com/photos/1592384/pexels-photo-1592384.jpeg?cs=srgb&dl=shallow-focus-photography-of-blue-alpine-car-1592384.jpg&fm=jpg',
  },
  {
    id: 10,
    name: 'Sal Monella',
    make: 'Audi',
    model: 'A7',
    year: '2015',
    imageUri:
      'https://images.pexels.com/photos/244206/pexels-photo-244206.jpeg?cs=srgb&dl=asphalt-auto-automobile-brand-244206.jpg&fm=jpg',
  },
  {
    id: 11,
    name: 'Sue Vaneer',
    make: 'chevrolet',
    model: 'corvette',
    year: '2017',
    imageUri:
      'https://images.pexels.com/photos/358070/pexels-photo-358070.jpeg?cs=srgb&dl=auto-automobile-automotive-car-358070.jpg&fm=jpg',
  },
  {
    id: 12,
    name: 'Cliff Hanger',
    make: 'Ford',
    model: 'Oldi',
    year: '1965',
    imageUri:
      'https://images.pexels.com/photos/2071/broken-car-vehicle-vintage.jpg?cs=srgb&dl=broken-car-vehicle-vintage-2071.jpg&fm=jpg',
  },
  {
    id: 13,
    name: 'Terry Aki',
    make: 'Chevrolet',
    model: 'Oldi',
    year: '1659',
    imageUri:
      'https://images.pexels.com/photos/712618/pexels-photo-712618.jpeg?cs=srgb&dl=blue-sedan-712618.jpg&fm=jpg',
  },
];
