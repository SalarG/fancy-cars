export default [
  {
    id: 1,
    available: 'In Dealership',
  },
  {
    id: 2,
    available: 'In Dealership',
  },
  {
    id: 3,
    available: 'Out of Stock',
  },
  {
    id: 4,
    available: 'Out of Stock',
  },
  {
    id: 5,
    available: 'Unavailable',
  },
  {
    id: 6,
    available: 'Unavailable',
  },
  {
    id: 7,
    available: 'Unavailable',
  },
  {
    id: 8,
    available: 'In Dealership',
  },
  {
    id: 9,
    available: 'Unavailable',
  },
  {
    id: 10,
    available: 'Out of Stock',
  },
  {
    id: 11,
    available: 'Out of Stock',
  },
  {
    id: 12,
    available: 'In Dealership',
  },
  {
    id: 13,
    available: 'In Dealership',
  },
];
