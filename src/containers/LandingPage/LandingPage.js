import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './LandingPage.style';
import {
  getCars,
  getAvailability,
  sortByName,
  sortByAvailablity,
  Alert,
} from '../../store/actions/actions';

const {width, height} = Dimensions.get('window');

class LandingPage extends Component {
  state = {
    isLoading: true,
    dropDownVisible: false,
    backgroundColor: {
      1: 'white',
      2: 'white',
      3: 'white',
      4: 'white',
      5: 'white',
    },
  };

  async componentDidMount() {
    await this.props.getCars();
    try {
      await setTimeout(() => {
        this.setState({isLoading: false});
      }, 2000);
    } catch (error) {
      // Alert.Alert([])
    }
  }
  renderItems = item => {
    return (
      <View style={styles.carImage}>
        <Image
          source={{
            uri: item.imageUri,
          }}
          style={styles.image}
          resizeMode={'contain'}
          width={width / 3}
        />
        <Text>{item.name}</Text>
        {item.available ? (
          <Text>{item.available}</Text>
        ) : (
          <TouchableOpacity
            onPress={() => {
              this.props.getAvailability(item.id);
            }}
            style={styles.availabilityButton}>
            <Text style={styles.buttonText}>Get availability</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };
  onPressDropDownExtend = () => {
    this.setState({dropDownVisible: !this.state.dropDownVisible});
  };

  handlePressSort = async category => {
    this.setState({isLoading: true, dropDownVisible: false});
    if (category === 'name') {
      await this.props.sortByName();
      return this.setState({isLoading: false});
    }
    await this.props.sortByAvailablity();
    return this.setState({isLoading: false});
  };
  handlePressRating = id => {
    let backgroundColorState = this.state.backgroundColor;
    Object.entries(backgroundColorState).map((entry, index) => {
      if (index <= id) {
        // entry = [id, 'red'];
        backgroundColorState[index] = 'red';
      }
    });
    console.log('new backgroundColor', backgroundColorState);
    this.setState({backgroundColor: backgroundColorState});
  };
  render() {
    // console.log('############', this.props.appState);
    return (
      <View style={styles.container}>
        <View style={styles.dropDownBox}>
          <View style={styles.sortByBox}>
            <Text
              style={styles.dropDownText}
              onPress={this.onPressDropDownExtend}>
              Sort by >
            </Text>
          </View>
          <View>
            <Text>Shopping Cart:</Text>
            <Text>{this.props.appState.shoppingCart}</Text>
          </View>
          {this.state.dropDownVisible && (
            <View style={styles.sortByBox}>
              <Text
                onPress={() => this.handlePressSort('name')}
                style={styles.dropDownText}>
                name
              </Text>
              <Text
                onPress={() => this.handlePressSort('availability')}
                style={styles.dropDownText}>
                availability
              </Text>
            </View>
          )}
        </View>
        <View style={styles.scrollerBox}>
          {!this.state.isLoading ? (
            <View style={{flex: 1}}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.props.appState.cars}
                renderItem={({item}) => {
                  return this.renderItems(item);
                }}
              />
              <View style={styles.ratingBox}>
                <TouchableOpacity onPress={() => this.handlePressRating(1)}>
                  <View
                    style={{
                      ...styles.star,
                      backgroundColor: this.state.backgroundColor[1],
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePressRating(2)}>
                  <View
                    style={{
                      ...styles.star,
                      backgroundColor: this.state.backgroundColor[2],
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePressRating(3)}>
                  <View
                    style={{
                      ...styles.star,
                      backgroundColor: this.state.backgroundColor[3],
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePressRating(4)}>
                  <View
                    style={{
                      ...styles.star,
                      backgroundColor: this.state.backgroundColor[4],
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePressRating(5)}>
                  <View
                    style={{
                      ...styles.star,
                      backgroundColor: this.state.backgroundColor[5],
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <ActivityIndicator size={'large'} color="green" />
          )}
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getCars: () => dispatch(getCars()),
    getAvailability: id => dispatch(getAvailability(id)),
    sortByName: () => dispatch(sortByName()),
    sortByAvailablity: () => dispatch(sortByAvailablity()),
  };
};

const mapStateToProps = state => {
  const {appState} = state;
  return {appState};
};
export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
