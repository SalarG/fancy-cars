import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    // flex: 1,
    paddingHorizontal: width * 0.02,
    borderWidth: 1,
  },
  dropDownBox: {
    width: width * 0.3,
    height: height * 0.1,
  },
  dropDownText: {
    fontSize: 20,
  },
  sortByBox: {
    backgroundColor: '#86bfeb',
  },
  carImage: {
    flex: 1,
    width: width,
    alignItems: 'center',
    borderWidth: 1,
  },
  imgaeContainer: {},
  image: {
    // width: width,
    height: width / 3,
  },
  scrollerBox: {
    height: height * 0.8,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  availabilityButton: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eb9786',
    height: height * 0.05,
    width: width / 3,
    paddingTop: 15,
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
  },
  ratingBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: height * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    borderWidth: 1,
    borderColor: 'red',
  },
  star: {
    width: width * 0.05,
    height: width * 0.05,
    // backgroundColor: 'orange',
    borderWidth: 1,
    marginHorizontal: width * 0.05,
  },
});
