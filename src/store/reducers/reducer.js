import {
  DATA_RECEIVED,
  AVAILABILITY_RECEIVED,
  SORTED_BY_NAME,
} from '../const/constants';

const initialState = {
  cars: [],
  shoppingCart: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DATA_RECEIVED:
      return {...state, cars: action.cars};
    case AVAILABILITY_RECEIVED:
      return {
        ...state,
        cars: action.cars,
        shoppingCart: action.newShoppingCart,
      };
    case SORTED_BY_NAME:
      return {...state, cars: action.cars};
    default:
      return state;
  }
};
