import {
  DATA_RECEIVED,
  AVAILABILITY_RECEIVED,
  SORTED_BY_NAME,
} from '../const/constants';
import availabilities from '../../data/availability';
import ServerModule from '../../server/servermodule';

export const getCars = () => {
  return async dispatch => {
    try {
      let cars = await ServerModule.GET('/cars');
      dispatch({type: DATA_RECEIVED, cars});
    } catch (error) {
      throw new error('error in getting data');
    }
  };
};

export const getAvailability = id => {
  return (dispatch, getState) => {
    try {
      let storeCars = getState().appState.cars;
      let available = ServerModule.GET(`/availability`, id);
      let shoppingCart = getState().appState.shoppingCart;
      console.log('shoppingCart', shoppingCart);

      // console.log('available', available === 'In Dealership');
      let newShoppingCart;
      if (available === 'In Dealership') {
        newShoppingCart = shoppingCart + 1;
        console.log('in DealerShip', newShoppingCart);
      } else {
        console.log('out of Stuck');
        newShoppingCart = shoppingCart;
      }

      console.log('shoppingCart', newShoppingCart);

      storeCars = storeCars.map(car => {
        if (car.id === id) {
          return {...car, available};
        }
        return car;
      });

      dispatch({
        type: AVAILABILITY_RECEIVED,
        cars: storeCars,
        newShoppingCart,
      });
    } catch (e) {
      throw new error('error in availabilty');
    }
  };
};

export const sortByName = () => {
  return (dispatch, getState) => {
    let storeCars = getState().appState.cars;
    storeCars = storeCars.sort((a, b) => {
      return ('' + a.name).localeCompare(b.name);
    });
    dispatch({type: SORTED_BY_NAME, cars: storeCars});
  };
};

export const sortByAvailablity = () => {
  return (dispatch, getState) => {
    let storeCars = getState().appState.cars;
    storeCars = storeCars.sort((a, b) => {
      return ('' + a.available).localeCompare(b.available);
    });
    dispatch({type: SORTED_BY_NAME, cars: storeCars});
  };
};
