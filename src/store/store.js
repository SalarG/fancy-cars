import {applyMiddleware, compose, createStore, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {rootReducer} from './reducers/index';
import {persistReducer, persistStore} from 'redux-persist';
import {AsyncStorage} from 'react-native';

// Using composeEnhancers,  REDUX can be debugged in  React native Debugger
let composeEnhancers = compose;
if (__DEV__) {
  // @ts-ignore
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: [],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  rootReducer,
  {},
  composeEnhancers(applyMiddleware(thunk)),
);
const persistor = persistStore(store);
//persistor.purge();
export {store};
